

import Foundation
// Codable-модель для парсинга JSON-объекта
final class JSONData: Codable {
    var response: Response
}

class Response: Codable {
    var items: [Item]
}

class Item: Codable {
    var sizes: [Size]
    var date: Int
}

class Size: Codable {
    var url: String
}
