

import Foundation
import RealmSwift
// модель Realm базы данных
final class PhotoDB: Object {
    @objc dynamic var imageUrl = ""
    @objc dynamic var date = 0
    
    override class func primaryKey() -> String? {
        return "imageUrl"
    }
}
