

import UIKit
import NotificationToast
// стартовый экран приложения
final class AuthViewController: UIViewController {
    @IBOutlet weak var buttonView: AuthButtonView! // кнопка логина ВК
    
    // инициализируем authService через синглтон
    var authService = SceneDelegate.shared().authService
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonView.delegate = self // устанавливаем VC делегатом кнопки
    }
}
// Auth Button Delegate
extension AuthViewController: AuthButtonViewDelegate {
    func buttonTapped() { // нажать кнопку
        authService?.wakeUpSession() // начать процедуру аутентификации
        // если нет соединения, отобразить тост
        if SceneDelegate.shared().validConnection == false {
            ToastView.showToastErrorConnection()
        }
    }
}
