//
//  TitleView.swift
//  VKPhotosLoader
//
//  Created by Max Yalounin on 10.08.2021.
//

import UIKit

@IBDesignable
final class TitleView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    var view: UIView! // сам вью
    let nibName = "TitleView" // наименование nib-файла
    
    // init-функции
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    // инициализация вью
    private func setupViews() {
        let xibView = loadFromXib()
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(xibView)
        setupLabel()
    }
    // загрузка вью из xib
    private func loadFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first! as! UIView
    }
    
    private func setupLabel() {
        titleLabel.text = "Mobile Up Gallery"
        titleLabel.font = UIFont(name: Fonts.bold, size: 48)

        
    }
}
