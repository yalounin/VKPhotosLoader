

import UIKit
// протокол для делегирования нажатия кнопки
protocol AuthButtonViewDelegate {
    func buttonTapped()
}
// кастомный вью с кнопкой
@IBDesignable
final class AuthButtonView: UIView {

    @IBOutlet weak var signInButton: UIButton! // кнопка
    var view: UIView! // сам вью
    let nibName = "AuthButtonView" // наименование nib-файла
    var delegate: AuthButtonViewDelegate? // делегат кнопки
    
    // init-функции
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    // инициализация вью
    private func setupViews() {
        let xibView = loadFromXib()
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(xibView)
        setupButton()
    }
    // загрузка вью из xib
    private func loadFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first! as! UIView
    }
    // нажатие кнопки через делегата
    @IBAction func signIn() {
        delegate?.buttonTapped()
    }
    // отрисовка кнопки
    private func setupButton() {
        signInButton.backgroundColor = .black
        signInButton.layer.cornerRadius = 10
        signInButton.setTitle("Вход через VK", for: .normal)
        signInButton.titleLabel?.font = UIFont(name: Fonts.regular, size: 18)
        signInButton.setTitleColor(.white, for: .normal)
        
        
    }
}
