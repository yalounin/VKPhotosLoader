
import UIKit
// xib-файл выбранного изображения
@IBDesignable
final class PhotoView: UIView {
    @IBOutlet weak var selectedPhotoImageView: UIImageView!
    var view: UIView! // сам вью
    let nibName = "PhotoView" // наименование nib-файла
    
    // init-функции
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    // инициализация вью
    private func setupViews() {
        let xibView = loadFromXib()
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(xibView)
    }
    // загрузка вью из xib
    private func loadFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first! as! UIView
    }
}
