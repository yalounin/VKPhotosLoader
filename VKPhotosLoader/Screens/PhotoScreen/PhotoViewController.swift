

import UIKit
import NotificationToast
import Kingfisher
// экран показа выбранного изображения
class PhotoViewController: UIViewController {
    // MARK: - PROPERTIES
    @IBOutlet weak var scrollView: UIScrollView! // скролл вью для зума
    @IBOutlet weak var collectionView: UICollectionView! // лента фотографий внизу экрана
    @IBOutlet weak var photoView: PhotoView! // аутлет изображения
    var photos = [PhotoDB]() // переданный массив всех фотографий
    var selectedPhoto: PhotoDB! // выбранное фото
    
    // MARK: - VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedPhotoConfigure()
        barItemsConfigure()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 3.0
        scrollView.delegate = self
    }
    // MARK: - IMAGE CONFIGURE
    // отобразить фото из URL
    func selectedPhotoConfigure() {
        scrollView.zoomScale = 1.0
        let url = URL(string: selectedPhoto.imageUrl) // формируем ссылку из строки
        photoView.selectedPhotoImageView.kf.setImage(with: url) // показываем изображение из url
    }
    // MARK: - NAV BAR CONFIGURE
    // инициализация айтемов navigationBar
    func barItemsConfigure() {
        barTitleConfigure()
        // кнопка Back
        self.navigationController?.navigationBar.tintColor = .black
        // кнопка Share
        let shareButton = UIBarButtonItem(
            title: nil,
            style: .plain,
            target: self,
            action: #selector(shareImage(sender:))
        )
        shareButton.image = UIImage(systemName: "square.and.arrow.up")
        self.navigationItem.rightBarButtonItem = shareButton
    }
    // инициализация тайтла
    private func barTitleConfigure() {
        // устанавливаем тайтлом дату в формате String
        let timeInterval = TimeInterval(selectedPhoto.date)
        let date = Date(timeIntervalSince1970: timeInterval)
        let df = DateFormatter()
        df.locale = Locale(identifier: "ru_RU")
        df.dateFormat = "d MMMM yyyy"
        let dateString = df.string(from: date)
        self.title = dateString
    }
    // MARK: - SHARE BUTTON
    // функционал кнопки "Share"
    @objc func shareImage(sender: UIBarButtonItem) {
        let sharedImage = self.photoView.selectedPhotoImageView.image
        
        let imageToShare = [ sharedImage! ]
        // создаем активити VC
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        // для iPad-ов
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        self.present(activityViewController, animated: true, completion: nil)
        
        activityViewController.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                        
            if (!completed) {
                return
            }
            guard error == nil else {
                ToastView.showToastErrorShare()
                return
            }
            ToastView.showToastSuccessShare()
        }
    }
}
// MARK: - COLLECTION VIEW DATA SOURCE
extension PhotoViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photos.count
    }
    // инициализация элементов collectionView
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID.photoScreen, for: indexPath) as? PhotoViewCell else {
            return UICollectionViewCell()
        }
        let photo = photos[indexPath.row]
        cell.configure(photo)
        return cell
    }
    // collectionView selection
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // обновляем selectedPhoto на выбранный в collectionView
        selectedPhoto = photos[indexPath.row]
        // обновляем изображение и тайтл
        selectedPhotoConfigure()
        barTitleConfigure()
    }
}
// MARK: - SCROLL VIEW DELEGATE
// image zooming
extension PhotoViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            return photoView
        }
}
