
import UIKit
import Kingfisher
// элемент collectionView на экране показа изображения
class PhotoViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoView: PhotoView! // аутлет изображения
    // заполнить ячейку данными из объекта PhotoDB
    func configure(_ photo: PhotoDB) {
        let url = URL(string: photo.imageUrl) // формируем ссылку из строки
        photoView.selectedPhotoImageView.kf.setImage(with: url) // показываем изображение из url
        photoView.selectedPhotoImageView.contentMode = .scaleAspectFill
    }
}
