

import UIKit
// Xib-файл для заполнения данными ячейки
@IBDesignable
final class PhotoCollectionViewCellXib: UIView {
    @IBOutlet weak var photoImageView: UIImageView! // аутлет изображения
    
    var view: UIView! // сам вью
    let nibName = "PhotoCollectionViewCell" // наименование nib-файла
    
    // init-функции
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    // инициализация вью
    private func setupViews() {
        let xibView = loadFromXib()
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(xibView)
    }
    // загрузка вью из xib
    private func loadFromXib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first! as! UIView
    }
}
