

import UIKit
import Kingfisher
// класс кастомной ячейки collectionView
final class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoView: PhotoCollectionViewCellXib! // аутлет изображения
    var photoImage: UIImage? { // изображение
        get {
            photoView.photoImageView.image // получаем его из imageView
        }
        set {
            photoView.photoImageView.image = newValue // обновляем imageView при обновлении изображения
        }
    }
    // заполнить ячейку данными из объекта PhotoDB
    func configure(_ photo: PhotoDB) {
        let url = URL(string: photo.imageUrl) // формируем ссылку из строки
        photoView.photoImageView.kf.setImage(with: url) // показываем изображение из url
    }
}
