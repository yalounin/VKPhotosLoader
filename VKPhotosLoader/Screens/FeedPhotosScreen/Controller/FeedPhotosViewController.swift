
import UIKit
import NotificationToast
// экран с миниатюрами фотографий
final class FeedPhotosViewController: UIViewController {
    // MARK: - PROPERTIES
    @IBOutlet weak var collectionView: UICollectionView! // аутлет collectionView
    let provider = DataProvider() // провайдер для получения данных
    // массив для заполнения данными
    var photos = [PhotoDB]() {
        didSet {
            collectionView.reloadData()
        }
    }
    // MARK: - VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        configureBarItemsUI() // отрисовываем элементы navigationBar
        
        // получаем данные из провайдера
        provider.fetchData(completion: { [weak self] array in
            guard let strongSelf = self else {
                return
            }
            strongSelf.photos = array // заполняем массив
            // если нет соединения, то отобразить тост
            if strongSelf.provider.validConnection == false {
                ToastView.showToastErrorConnection()
            }
        })
    }
    // MARK: - VIEW WILL DISAPPEAR
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationItem.title = "" // убираем текст из тайтла
    }

    // MARK:- NAVIGATIONBAR UI
    // отрисовка navigationBar
    private func configureBarItemsUI() {
        navigationController?.navigationBar.barTintColor = .white // цвет бара
        title = "Mobile Up Gallery" // тайтл
        // шрифт тайтла
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: Fonts.medium, size: 18)!]
        // кнопка "Выход"
        let b = UIBarButtonItem(
            title: "Выход",
            style: .plain,
            target: self,
            action: #selector(signOut(sender:))
        )
        // шрифт и цвет текста кнопки
        b.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: Fonts.medium, size: 18)!, NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        navigationItem.rightBarButtonItem = b
    }
    // MARK: - LOGOUT BUTTON
    // функционал кнопки "Выход"
    @objc func signOut(sender: UIBarButtonItem) {
        // рисуем alertController для подтверждения действия
        let ac = UIAlertController(title: "Вы уверены?", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "Да", style: .default, handler: {_ in
            // достаем authService из SceneDelegate
            let authService = SceneDelegate.shared().authService!
            authService.logOut() // закончить сессию
            // отобразить стартовый VC
            let authVC = UIStoryboard(name: "AuthViewController", bundle: nil).instantiateInitialViewController() as! AuthViewController
            SceneDelegate.shared().window?.rootViewController = authVC
        })
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        ac.addAction(cancel)
        ac.addAction(action)
        present(ac, animated: true, completion: nil)
    }
    

}
// MARK: - COLLECTIONVIEW DATA SOURCE
extension FeedPhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photos.count // количество элементов в массиве
    }
    // заполняем ячейки данными из массива
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellID.feedPhotosScreen, for: indexPath) as? PhotoCollectionViewCell else {
            return UICollectionViewCell()
        }
        let photo = photos[indexPath.row]
        cell.configure(photo)
        return cell
    }
    // функция для отображения данных в 2 ряда
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
          let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
          let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
          return CGSize(width: size, height: size)
      }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photoVC = UIStoryboard(name: "PhotoViewController", bundle: nil).instantiateInitialViewController() as! PhotoViewController
        photoVC.selectedPhoto = photos[indexPath.row]
        photoVC.photos = photos
        self.navigationController?.pushViewController(photoVC, animated: true)
    }
}
