

import Foundation
import RealmSwift
// класс для кэширования и получения данных из БД
final class DBService {
    // инициализируем объект БД
    let realm = try! Realm()
    
    // MARK: Server -> DB
    // функция для сохранения данных с сервера в БД
    func cache(from data: JSONData) {
            try! self.realm.write {
                self.realm.deleteAll() // удаляем все элементы
                // добавляем данные из JSON-модели
                for element in data.response.items {
                    let photo = PhotoDB()
                    photo.date = element.date
                    photo.imageUrl = element.sizes.last!.url
                    self.realm.add(photo, update: .all)
                }
            }
    }
    
    // MARK: DB -> PROVIDER
    // получить данные из БД для передачи в DataProvider
    func getObjects() -> [PhotoDB] {
        var photosArray = [PhotoDB]()
            for element in self.realm.objects(PhotoDB.self) {
                photosArray.append(element)
            }
        // возвращаем отсортированный по дате массив
        return photosArray.sorted(by: {$0.date < $1.date})
    }
}
