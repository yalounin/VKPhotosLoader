

import Foundation

// класс для получения данных с сервера и кэширования их в БД
final class DataProvider {
    var validConnection: Bool! // есть ли соединение
    // создаем экземпляры сервисов
    let server = ServerService()
    let db = DBService()

    // получить кжшированные данные
    func fetchData(completion: (([PhotoDB]) -> Void)?) {
        var photosArray = [PhotoDB]() // создаем пустой массив
        validConnection = false // по умолчанию соединения с сетью нет

        // делаем запрос на сервер
        server.fetchData(completion: { [weak self] photos in

            guard let strongSelf = self else {
                return
            }
            DispatchQueue.main.async {
                // если полученные данные не nil, то обновляем БД
                if let photos = photos {
                    strongSelf.db.cache(from: photos)
                    strongSelf.validConnection = true // соединение с сетью есть
                }
                // получаем данные из БД и передаем в комплишн
                photosArray = strongSelf.db.getObjects()
                completion?(photosArray)
            }   
        })
    }
}
