

import Foundation
// класс для запроса данных с сервера
final class ServerService {
    private let authService: AuthService
    // определяем authService в инициализаторе
    init() {
        authService = SceneDelegate.shared().authService
    }
    // сформировать URL
    func getUrl() -> URL? {
        var components = URLComponents()
        // токен достаем из authService
        guard let token = authService.token else {return nil}
        // инициализируем параметры запроса
        components.scheme = RequestAPI.scheme
        components.host = RequestAPI.host
        components.path = RequestAPI.getPhotos
        var params = [String:String]()
        params["access_token"] = token
        params["owner_id"] = RequestAPI.ownerID
        params["album_id"] = RequestAPI.albumID
        params["v"] = RequestAPI.apiVersion
        
        components.queryItems = params.map({URLQueryItem(name: $0, value: $1)})
        
        guard let url = components.url else {return nil}
        
        return url
    }
    // запросить данные с сервера
    func fetchData(completion: ((JSONData?) -> Void)?) {
        
        guard let url = getUrl() else {return} // полученный url
        let request = URLRequest(url: url) // формируем запрос
        // делаем запрос на сервер
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            // в случае ошибки или отсутствия данных, передаем в комплишн nil
            guard let data = data, error == nil else {
                completion?(nil)
                return
            }
            // декодируем через Codable-модель
            let json = try? JSONDecoder().decode(JSONData.self, from: data)
            completion?(json)
        }
        task.resume()
    }
}
