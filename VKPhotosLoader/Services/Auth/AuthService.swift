

import Foundation
import VK_ios_sdk
// протокол для делегирования
protocol AuthServiceDelegate: class {
    func authServiceShouldShow(_ viewController: UIViewController)
    func authServiceSignIn()
    func authServiceSignInDidFail()
}

final class AuthService: NSObject, VKSdkDelegate, VKSdkUIDelegate {
    
    private let appId = "7919655" // app id приложения в интерфейсе VK
    private let vkSdk: VKSdk // переменная для управления VK SDK
    
    weak var delegate: AuthServiceDelegate?
    // токен, полученный в результате успешной авторизации VK
    var token: String? {
        return VKSdk.accessToken()?.accessToken
    }
    
    override init() {
        vkSdk = VKSdk.initialize(withAppId: appId) // инициализируем SDK
        super.init()
        // устанавливаем делегатов
        vkSdk.register(self)
        vkSdk.uiDelegate = self
    }
    // начать процесс аутентификации через SDK
    func wakeUpSession() {
        let scope = ["offline"]
        
        VKSdk.wakeUpSession(scope, complete: { [delegate] (state, error) in
 
            if state == .initialized { // если еще юзер не авторизован
                VKSdk.authorize(scope) // запустить процесс авторизации
            } else if state == .authorized { // если юзер авторизован
                delegate?.authServiceSignIn()
            } else { // если ошибка аутентификации
                delegate?.authServiceSignInDidFail()
            }
        })
    }
    
    func logOut() {
        VKSdk.forceLogout()
    }
    
    // MARK: - VKSDK DELEGATE
    // если аутентификация завершена успешно
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        print(#function)
        if result.token != nil {
            delegate?.authServiceSignIn()
        }
    }
    // если аутентификация завершена ошибочно
    func vkSdkUserAuthorizationFailed() {
        print(#function)
        delegate?.authServiceSignInDidFail()
    }
    
    // MARK: - VKSDK UI DELEGATE
    // отобразить окно авторизации
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        print(#function)
        delegate?.authServiceShouldShow(controller)
    }
    // функция ввода капчи
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        print(#function)
    }
}
