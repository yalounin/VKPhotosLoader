
import UIKit
import VK_ios_sdk

class SceneDelegate: UIResponder, UIWindowSceneDelegate, AuthServiceDelegate {
    // синглтон
    static func shared() -> SceneDelegate {
        let scene = UIApplication.shared.connectedScenes.first
        let sd: SceneDelegate = (((scene?.delegate as? SceneDelegate)!))
        return sd
    }
    var validConnection: Bool?
    var authService: AuthService! // переменная для аутентификации
    var window: UIWindow? // экран приложения
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // инициализируем authService, устанавливаем делегата
        self.authService = AuthService()
        authService.delegate = self
        validConnection = true

        // запускаем сториборд с окном аутентификации
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        let authVC = UIStoryboard(name: "AuthViewController", bundle: nil).instantiateInitialViewController() as? AuthViewController
        window?.windowScene = windowScene
        window?.rootViewController = authVC
        window?.makeKeyAndVisible()
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if let url = URLContexts.first?.url {
            VKSdk.processOpen(url, fromApplication: UIApplication.OpenURLOptionsKey.sourceApplication.rawValue)
        }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }

    // при отсутствии токена запускаем viewController с ВК-авторизацией
    func authServiceShouldShow(_ viewController: UIViewController) {
        print(#function)
        validConnection = true
        window?.rootViewController?.present(viewController, animated: true, completion: nil)
    }
    
    // при полученном токене открываем VC с фотографиями
    func authServiceSignIn() {
        print(#function)
        let feedVC = UIStoryboard(name: "FeedPhotosViewController", bundle: nil).instantiateInitialViewController() as! FeedPhotosViewController
        let navVC = UINavigationController(rootViewController: feedVC)
        window?.rootViewController = navVC
    }
    
    // действия при неудачной аутентификации
    func authServiceSignInDidFail() {
        print(#function)
        validConnection = false
    }
}

