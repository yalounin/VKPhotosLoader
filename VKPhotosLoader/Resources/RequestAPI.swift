

import Foundation
// класс со статическими параметрами для формирования HTTP-запроса
final class RequestAPI {
    static let scheme = "https"
    static let host = "api.vk.com"
    static let getPhotos = "/method/photos.get"
    static let ownerID = "-128666765"
    static let albumID = "266276915"
    static let apiVersion = "5.131"
}
