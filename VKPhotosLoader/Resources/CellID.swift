

import Foundation

struct CellID {
    static let feedPhotosScreen = "Cell"
    static let photoScreen = "PhotoCell"
}
