

import Foundation
// структура с названиями шрифтов
struct Fonts {
    static let regular = "SFProDisplay-Regular"
    static let bold = "SFProDisplay-Bold"
    static let medium = "SFProDisplay-Medium"
}
