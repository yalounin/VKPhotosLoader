

import NotificationToast
// показать тост c текстом
extension ToastView {
    static func showToastErrorConnection() {
        let toast = ToastView(title: "Проблемы с сетью!")
        toast.displayTime = 3
        toast.show()
    }
    
    static func showToastErrorShare() {
        let toast = ToastView(title: "Ошибка!")
        toast.displayTime = 3
        toast.show()
    }
    
    static func showToastSuccessShare() {
        let toast = ToastView(title: "Успешно!")
        toast.displayTime = 3
        toast.show()
    }
    
}
