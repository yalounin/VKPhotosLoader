Тестовое приложение для Mobile Up - VKPhotosLoader
Цель: отображение изображений из группы ВК. В проекте реализована OAuth-аутентификация через VK SDK, кэширование в локальную БД и возможность сохранять и делиться изображениями.

Stack:
Swift
UIKit
Alamofire
Realm
Codable
NotificationToast
VKSDK
Kingfisher

Architecture: Model-View-Controller
